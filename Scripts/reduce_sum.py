import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

import numpy as np
import tensorflow as tf

x=tf.constant([[1, 2], [3, 4], [5, 6]])
y=tf.reduce_sum(x)
sess = tf.Session()
print(sess.run(y))

