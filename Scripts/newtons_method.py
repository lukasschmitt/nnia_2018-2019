import numpy as np

x = np.array([1,1])
n_iterations = 50
hessian = np.array([[2,1],[1,2]])

for n in range(1,n_iterations+1):
	gradient = np.array([2*x[0]-3-x[1],2*x[1]-x[0]])
	x = np.subtract(x,np.matmul(hessian,gradient))
	print(x)