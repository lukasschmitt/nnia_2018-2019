import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

import numpy as np
import tensorflow as tf


a = tf.constant(30,name="a")
b = tf.constant(12,name="b")
c = tf.constant(1,name="c")
x = tf.add(a, b,name="x")
y = tf.add(a, c,name="y")
z = tf.add(x, y,name="z")
with tf.Session() as sess:
	writer = tf.summary.FileWriter('./log', sess.graph) 
	print(sess.run(z))
	print(sess.run(y))
	print(sess.run(x))
writer.close()

