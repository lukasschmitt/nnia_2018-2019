'''
This code is an example for one dimensional gradient descent

Author: Dietrich Klakow 
'''
#Project: https://repos.lsv.uni-saarland.de/dietrich/Neural_Networks_Implementation_and_Application/tree/master

from math import pow

def f( x ):
	return x*x+x*x*x+pow(x, 4)


def fp( x ):
	return 2*x+3*x*x+4*pow(x, 3)

# Initial point
x=5.6

#Learning rate
#epsilon=0.007
epsilon=0.02
#try also 0.02 to see what happens

for i in range(0, 50):
	print("x: ", x, " f(x): ", f(x))
	x = x - epsilon * fp(x)


