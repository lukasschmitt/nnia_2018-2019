import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

import numpy as np
import tensorflow as tf

dim = 2
A = tf.constant([[2.0, 1.0], [1.0, 2.0]])
b = tf.constant([[1.0, 1.0]])
c = tf.constant([[0.1]])
x = tf.transpose(tf.Variable([[1.0,1.0]]))
# quadratic function: f(x) = 1/2 * x^t A x + b^t x + c
fx = 0.5 * tf.matmul(tf.matmul(tf.transpose(x), A), x) + tf.matmul(b, x) + c
dfx = tf.gradients(fx, x)[0]
  
for i in range(dim):
# tf.slice extracts a slice from a tensor
	dfx_i = tf.slice(dfx, begin=[i,0] , size=[1,1])
	ddfx_i = tf.gradients(dfx_i, x)[0] 
	if i == 0: hess = ddfx_i
# tf.concat concatenates tensors along one dimension
	else: hess = tf.concat([hess, ddfx_i],1) 
  
init_op = tf.global_variables_initializer()
    
with tf.Session() as sess:
	sess.run(init_op)
	print(sess.run(hess))
