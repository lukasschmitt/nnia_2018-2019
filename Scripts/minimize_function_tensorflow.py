import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

import tensorflow as tf
import numpy as np
import math


x = tf.Variable([.3], tf.float32)

cost = (x-1.0)*(x-1.0)
opt = tf.train.GradientDescentOptimizer(learning_rate=0.1)
train = opt.minimize(cost)

with tf.Session() as sess:
    tf.initialize_all_variables().run()
    print (sess.run(x), sess.run(cost))

    for i in range(20):
        sess.run(train)
        print ( sess.run(x), sess.run(cost) )
