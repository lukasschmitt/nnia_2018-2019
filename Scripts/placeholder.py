import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

import numpy as np
import tensorflow as tf

a = tf.placeholder(tf.float32)
b = tf.constant(30, tf.float32)
x = a + b 

with tf.Session() as sess:
	writer = tf.summary.FileWriter('./log', sess.graph)
	print(sess.run(x, {a: 12})) 
	replace_dict = {a: -10}
	print(sess.run(x, feed_dict=replace_dict)) 
writer.close()

