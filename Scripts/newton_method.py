'''
This code is an example for one dimentional newton method

Author: Dietrich Klakow based
'''
#Project: https://repos.lsv.uni-saarland.de/dietrich/Neural_Networks_Implementation_and_Application/tree/master

from math import pow

def f( x ):
	return x*x+x*x*x+pow(x, 4)

def fp( x ):
	return 2*x+3*x*x+4*pow(x, 3)

def fpp( x ):
	return 2+6*x+12*pow(x, 2)

# Initial point
x=5.6

#Learning rate
epsilon=1

for i in range(0, 30):
	print ( "x: ", x, " f(x): ", f(x) )
	x = x - epsilon * fp(x)/fpp(x)


