import numpy as np 
import math

x = np.array([1,1])
n_iterations = 10

for n in range(1,n_iterations+1):
	gradient = np.array([0.5*(-(2*x[0]-3-x[1])),0.5*(-(2*x[1]-x[0]))])
	l2_norm = np.linalg.norm(gradient)
	if l2_norm >= 0.2:
		x1 = x[0]+gradient[0]
		x2 = x[1]+gradient[1]
		x = np.array([x1,x2])
		print("Gradient: " + str(gradient))
		print("L2-Norm: " + str(l2_norm))
		print("New vector after "+ str(n) +". iteration: (" + str(x1) + "," + str(x2) + ")\n")
	else:
		print("It stopped after iteration " + str(n) + "!")
		print("L2-Norm in last iteration: " + str(l2_norm))
		print("Gradient in last iteration: " + str(gradient))
		break

