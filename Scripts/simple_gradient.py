import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

import numpy as np
import tensorflow as tf

x = tf.Variable(2.0)
y = 2.0 * (x ** 3)
grad_y = tf.gradients(y, x)
with tf.Session() as sess:
	writer = tf.summary.FileWriter('./log', sess.graph)
	sess.run(x.initializer)
	print(sess.run(grad_y))
writer.close()
